/**
 * Pro16CallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package generated;


/**
 *  Pro16CallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class Pro16CallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public Pro16CallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public Pro16CallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for getLastAdvice method
     * override this method for handling normal response from getLastAdvice operation
     */
    public void receiveResultgetLastAdvice(
        generated.Pro16Stub.GetLastAdviceResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getLastAdvice operation
     */
    public void receiveErrorgetLastAdvice(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for forgetAdvices method
     * override this method for handling normal response from forgetAdvices operation
     */
    public void receiveResultforgetAdvices() {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from forgetAdvices operation
     */
    public void receiveErrorforgetAdvices(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getHealth method
     * override this method for handling normal response from getHealth operation
     */
    public void receiveResultgetHealth(
        generated.Pro16Stub.GetHealthResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getHealth operation
     */
    public void receiveErrorgetHealth(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getAdvice method
     * override this method for handling normal response from getAdvice operation
     */
    public void receiveResultgetAdvice(
        generated.Pro16Stub.GetAdviceResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAdvice operation
     */
    public void receiveErrorgetAdvice(java.lang.Exception e) {
    }
}
