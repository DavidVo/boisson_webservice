package fr.greta.formation.dglv.boisson.main;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import generated.Pro16Stub;
import generated.Pro16Stub.Food;
import generated.Pro16Stub.GetAdvice;
import generated.Pro16Stub.GetAdviceResponse;
import generated.Pro16Stub.GetHealth;
import generated.Pro16Stub.GetHealthResponse;

public class BeveragesClient {

	public static void main(String[] args) throws RemoteException {
		
		Pro16Stub stub = new Pro16Stub();
		
		GetHealth getHealth= new GetHealth();
		GetHealthResponse getHealthResponse = stub.getHealth(getHealth);
		System.out.println(getHealthResponse.get_return());
		
		GetAdvice getAdvice = new GetAdvice();
		getAdvice.setFd(Food.Main);
		GetAdviceResponse getAdviceResponse = stub.getAdvice(getAdvice);
		System.out.println(getAdviceResponse.get_return());
		

	}

}
