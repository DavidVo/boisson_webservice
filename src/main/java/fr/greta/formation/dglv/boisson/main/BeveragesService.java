package fr.greta.formation.dglv.boisson.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BeveragesService {
	
	public enum Food {Aperitif,Starter,Main,Dessert,other};
	private List<String> lastAdvice = new ArrayList<String>();
	
	public String getHealth() {
		return "ok";
	}
	
	public String getAdvice(Food fd) {
		String adv = "Villagoise";
		
		if (fd==Food.Aperitif) adv = "Mojito";
		if (fd==Food.Starter) adv = "White wine";
		if (fd==Food.Main) adv = "Red wine";
		if (fd==Food.Dessert) adv = "Whisky";
		
		lastAdvice.add(adv);
		if (lastAdvice.size() > 3) lastAdvice.remove(0);
		
		return adv;
	}
	
	public List<String> getLastAdvice() {
		return lastAdvice;
	}
	
	public void forgetAdvices() {
		lastAdvice.clear();
	}
	
}
