package boisson_webService;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.greta.formation.dglv.boisson.main.BeveragesService;

public class BeverageServiceTest {
	
	@Test
	public void AdviceTest() {
		BeveragesService bs=new BeveragesService();
		assertEquals("Mojito", bs.getAdvice(BeveragesService.Food.Aperitif));
		assertEquals("White wine", bs.getAdvice(BeveragesService.Food.Starter));
		assertEquals("Villagoise", bs.getAdvice(BeveragesService.Food.other));
	}
	
	@Test
	public void HealthTest() {
		BeveragesService bs=new BeveragesService();
		assertEquals("ok", bs.getHealth());
	}
	

}
